import time
import requests
import json
import threading
import flask
import socket

from flask import request

import swagger_client
from swagger_client.configuration import Configuration

from config import BaseConfig

PROXY_URI = f'{BaseConfig.PROXY_SERVICE}:{BaseConfig.PROXY_PORT}'
REST_URI = f'api.{PROXY_URI}'
REDIRECTOR_URI = f'{PROXY_URI}'
WEB_URI = f'app.{PROXY_URI}'
DATA_URI = f'data.{PROXY_URI}'

REST_ENDPOINT = f'http://{REST_URI}/v3'
REDIRECTOR_ENDPOINT = f'http://{REDIRECTOR_URI}'

CALLBACK_PORT = 8000
CALLBACK_ENDP = '/endp'
CALLBACK_HOSTNAME = socket.gethostname()
CALLBACK_ENDPOINT = f'http://{CALLBACK_HOSTNAME}:{CALLBACK_PORT}{CALLBACK_ENDP}'

TIMEOUT_CHECKER = 60
TIMEOUT_HOOK = 120

# Create an instance of the API class
conf = Configuration()
conf.host = REST_ENDPOINT
api_client = swagger_client.ApiClient(conf)
api_user = swagger_client.UserApi(api_client)
api_uri = swagger_client.UriApi(api_client)
api_hook = swagger_client.HookApi(api_client)

callback_cnd = threading.Condition()
callback_method = 'GET'
callback_param_key = 'hey'
callback_param_value = 'there'
callback_params = f'{callback_param_key}={callback_param_value}'
callback_body = {'data': {'nested': 'param'}}
callback_clicks = 5
callback_minutes = 5

errors = 0

new_user_mail = 'new_user@mail.com'
new_user_pass = 'new_user'
new_user_id = None
new_user_token = None

new_uri_real = 'https://google.com'
new_uri_id = None

s = requests.Session()


def result(good):
    global errors
    if good:
        print('Done', flush=True)
    else:
        print('Fail', flush=True)
        errors += 1


print('======================')
print('== NEXUS TEST SUITE ==')
print('======================')

print('\n------------------------------------------------')
print('Test redirector with default URI')

response = requests.get(f'http://{REDIRECTOR_URI}/abc5', allow_redirects=False)
correct = response.ok and \
	response.headers['Location'] == 'https://twitter.com'
result(correct)

print('\n------------------------------------------------')
print('Test login with default user')

john_user_req = swagger_client.UserRequest(email='john@mail.com', _pass='john')

try:
    res_user_login = api_user.login_user(body=john_user_req, async_req=False)

    correct = res_user_login.user.email == 'john@mail.com'
    result(correct)
except Exception as e:
    print("Exception when calling the API: %s\n" % e)
    errors += 1

print('\n------------------------------------------------')
print('Test create user')

new_user_req = swagger_client.UserRequest(email=new_user_mail, _pass=new_user_pass)

try:
    res_user_login = api_user.create_user(body=new_user_req, async_req=False)
    correct = res_user_login.user.email == new_user_mail
    if correct:
        new_user_id = res_user_login.user.user_id

    result(correct)
except Exception as e:
    print("Exception when calling the API: %s\n" % e)
    errors += 1

print('\n------------------------------------------------')
print('Test login with new user')

data = {'email': new_user_mail, 'pass': new_user_pass}
try:
    res_user_login = s.post(
        f'{REST_ENDPOINT}/users/login',
        json=data
    )
    user = res_user_login.json()['user']
    correct = user['email'] == new_user_mail and \
              user['userId'] == new_user_id
    new_user_token = res_user_login.json()['token']
    result(correct)

    if correct:
        s.headers.update({'Authorization': f'Bearer {new_user_token}'})
except Exception as e:
    print("Exception when calling the API: %s\n" % e)
    errors += 1

print('\n------------------------------------------------')
print('Test create URI')

data = {'realURI': new_uri_real}

try:
    res_uri = s.post(
        f'{REST_ENDPOINT}/uris',
        json=data
    )
    uri = res_uri.json()
    correct = uri['realURI'] == new_uri_real and \
              uri['userId'] == new_user_id
    if correct:
        new_uri_id = uri['shortURI']
    result(correct)
except Exception as e:
    print("Exception when calling the API: %s\n" % e)
    errors += 1

print('\n------------------------------------------------')
print('Test redirector with new URI (expect not accessible)')

try:
    res = s.get(
        f'{REDIRECTOR_ENDPOINT}/{new_uri_id}',
        allow_redirects=False
    )
    correct = not res.ok
    result(correct)
except Exception as e:
    print("Exception when calling the API: %s\n" % e)
    errors += 1

print('\n------------------------------------------------')
print('Test create Hook')

data = {
  "actionsHTTP": [
    {
      "body": callback_body,
      "endpoint": CALLBACK_ENDPOINT,
      "params": callback_params,
      "verb": callback_method
    }
  ],
  "actionsMail": [
    {
      "destinations": [
        "destinations",
        "destinations"
      ],
      "subject": "subject",
      "text": "text"
    }
  ],
  "trigger": {
    "clicks": callback_clicks,
    "minutes": callback_minutes
  },
  "uriId": new_uri_id
}

try:
    res_hook = s.post(
        f'{REST_ENDPOINT}/hooks',
        json=data
    )
    hook = res_hook.json()
    correct = hook['uriId'] == new_uri_id and \
              hook['userId'] == new_user_id and \
              hook['actionsHTTP'][0]['endpoint'] == CALLBACK_ENDPOINT

    result(correct)
except Exception as e:
    print("Exception when calling the API: %s\n" % e)
    errors += 1

print('\n------------------------------------------------')
print('Test redirector with new URI (expect accessible)')

correct = False
initial = time.time()

try:
    while not correct and (time.time() - initial) < TIMEOUT_CHECKER:
        time.sleep(1)
        res = s.get(
            f'{REDIRECTOR_ENDPOINT}/{new_uri_id}',
            allow_redirects=False
        )
        correct = res.ok and \
        	res.headers['Location'] == new_uri_real

    print(f'Elapsed {time.time() - initial} seconds')

    result(correct)
except Exception as e:
    print("Exception when calling the API: %s\n" % e)
    errors += 1

print('\n------------------------------------------------')
print('Test hook HTTP action')


def run_tests(app):
    @app.route(CALLBACK_ENDP, methods=[callback_method])
    def endp():
        if request.method == callback_method and request.args[callback_param_key] == callback_param_value:
            with callback_cnd:
                callback_cnd.notify()
            return 'Ok'
        else:
            return 'Bad'

    endpoints = threading.Thread(target=start_server, args=(app,), daemon=True)
    endpoints.start()


def start_server(app):
    # This is needed because the simple devel server of Flask can't be ran in a thread
    from tornado.wsgi import WSGIContainer
    from tornado.web import FallbackHandler, Application
    from tornado.ioloop import IOLoop
    import asyncio

    asyncio.set_event_loop(asyncio.new_event_loop())
    cont = WSGIContainer(app)
    application = Application([
        (r"/(.*)", FallbackHandler, dict(fallback=cont)),
    ])
    application.listen(CALLBACK_PORT)
    IOLoop.instance().start()


app = flask.Flask(__name__)
run_tests(app)
try:
    for i in range(callback_clicks):
        res = s.get(
            f'{REDIRECTOR_ENDPOINT}/{new_uri_id}',
            allow_redirects=False
        )
        time.sleep(0.2)

    with callback_cnd:
        correct = callback_cnd.wait(timeout=TIMEOUT_HOOK)

    result(correct)
except Exception as e:
    print("Exception when calling the API: %s\n" % e)
    errors += 1

print('\n================================================')
if errors == 0:
    print('\nTests completed successfully\n')
    exit(0)
else:
    print(f'\nTests finished with {errors} errors\n')
    exit(1)
