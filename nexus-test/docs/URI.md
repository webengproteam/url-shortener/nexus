# URI

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uri_id** | **str** |  | [optional] 
**user_id** | **int** |  | [optional] 
**real_uri** | **str** |  | [optional] 
**short_uri** | **str** |  | [optional] 
**activated** | **bool** |  | [optional] 
**reachable** | **bool** |  | [optional] 
**safe** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

