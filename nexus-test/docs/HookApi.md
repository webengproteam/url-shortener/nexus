# swagger_client.HookApi

All URIs are relative to */v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**activate_hook**](HookApi.md#activate_hook) | **PUT** /hooks/{hookId}/activate | Activate a hook
[**create_hook**](HookApi.md#create_hook) | **POST** /hooks | Create hook
[**deactivate_hook**](HookApi.md#deactivate_hook) | **PUT** /hooks/{hookId}/deactivate | Deactivate a hook
[**delete_hook**](HookApi.md#delete_hook) | **DELETE** /hooks/{hookId} | Deletes a hook
[**get_hook**](HookApi.md#get_hook) | **GET** /hooks/{hookId} | Retrieve a hook
[**get_hooks_by_uri**](HookApi.md#get_hooks_by_uri) | **GET** /uris/{uriId}/hooks | Retrieves all the hooks of a URI
[**update_hook**](HookApi.md#update_hook) | **PUT** /hooks/{hookId} | Update hook

# **activate_hook**
> Hook activate_hook(hook_id)

Activate a hook

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint


# create an instance of the API class
api_instance = swagger_client.HookApi(swagger_client.ApiClient(configuration))
hook_id = 789 # int | ID of hook to activate

try:
    # Activate a hook
    api_response = api_instance.activate_hook(hook_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling HookApi->activate_hook: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **hook_id** | **int**| ID of hook to activate | 

### Return type

[**Hook**](Hook.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_hook**
> Hook create_hook(body)

Create hook

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint


# create an instance of the API class
api_instance = swagger_client.HookApi(swagger_client.ApiClient(configuration))
body = swagger_client.HookRequest() # HookRequest | Created hook object

try:
    # Create hook
    api_response = api_instance.create_hook(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling HookApi->create_hook: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**HookRequest**](HookRequest.md)| Created hook object | 

### Return type

[**Hook**](Hook.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **deactivate_hook**
> Hook deactivate_hook(hook_id)

Deactivate a hook

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint


# create an instance of the API class
api_instance = swagger_client.HookApi(swagger_client.ApiClient(configuration))
hook_id = 789 # int | ID of hook to deactivate

try:
    # Deactivate a hook
    api_response = api_instance.deactivate_hook(hook_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling HookApi->deactivate_hook: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **hook_id** | **int**| ID of hook to deactivate | 

### Return type

[**Hook**](Hook.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_hook**
> delete_hook(hook_id)

Deletes a hook

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint


# create an instance of the API class
api_instance = swagger_client.HookApi(swagger_client.ApiClient(configuration))
hook_id = 789 # int | Hook id to delete

try:
    # Deletes a hook
    api_instance.delete_hook(hook_id)
except ApiException as e:
    print("Exception when calling HookApi->delete_hook: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **hook_id** | **int**| Hook id to delete | 

### Return type

void (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_hook**
> Hook get_hook(hook_id)

Retrieve a hook

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint


# create an instance of the API class
api_instance = swagger_client.HookApi(swagger_client.ApiClient(configuration))
hook_id = 789 # int | ID of Hook to retrieve

try:
    # Retrieve a hook
    api_response = api_instance.get_hook(hook_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling HookApi->get_hook: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **hook_id** | **int**| ID of Hook to retrieve | 

### Return type

[**Hook**](Hook.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_hooks_by_uri**
> list[Hook] get_hooks_by_uri(uri_id)

Retrieves all the hooks of a URI

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint


# create an instance of the API class
api_instance = swagger_client.HookApi(swagger_client.ApiClient(configuration))
uri_id = 'uri_id_example' # str | ID of URI to get hooks from

try:
    # Retrieves all the hooks of a URI
    api_response = api_instance.get_hooks_by_uri(uri_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling HookApi->get_hooks_by_uri: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uri_id** | **str**| ID of URI to get hooks from | 

### Return type

[**list[Hook]**](Hook.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_hook**
> Hook update_hook(body, hook_id)

Update hook

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint


# create an instance of the API class
api_instance = swagger_client.HookApi(swagger_client.ApiClient(configuration))
body = swagger_client.HookRequest() # HookRequest | Created hook object
hook_id = 789 # int | ID of Hook to retrieve

try:
    # Update hook
    api_response = api_instance.update_hook(body, hook_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling HookApi->update_hook: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**HookRequest**](HookRequest.md)| Created hook object | 
 **hook_id** | **int**| ID of Hook to retrieve | 

### Return type

[**Hook**](Hook.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

