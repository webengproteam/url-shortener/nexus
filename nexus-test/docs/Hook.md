# Hook

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hook_id** | **int** |  | [optional] 
**user_id** | **int** |  | [optional] 
**uri_id** | **str** |  | [optional] 
**active** | **bool** |  | [optional] 
**trigger** | [**HookTrigger**](HookTrigger.md) |  | [optional] 
**actions_http** | [**list[HookActionHTTP]**](HookActionHTTP.md) |  | [optional] 
**actions_mail** | [**list[HookActionMail]**](HookActionMail.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

