# swagger_client.UriApi

All URIs are relative to */v3*

Method | HTTP request | Description
------------- | ------------- | -------------
[**activate_uri**](UriApi.md#activate_uri) | **PUT** /uris/{uriId}/activate | Activate a URI
[**add_uri**](UriApi.md#add_uri) | **POST** /uris | Add a new URI
[**deactivate_uri**](UriApi.md#deactivate_uri) | **PUT** /uris/{uriId}/deactivate | Deactivate a URI
[**delete_uri**](UriApi.md#delete_uri) | **DELETE** /uris/{uriId} | Deletes a URI
[**get_hooks_by_uri**](UriApi.md#get_hooks_by_uri) | **GET** /uris/{uriId}/hooks | Retrieves all the hooks of a URI
[**get_uri**](UriApi.md#get_uri) | **GET** /uris/{uriId} | Find URI by ID
[**get_uri_by_user**](UriApi.md#get_uri_by_user) | **GET** /users/{userId}/uris | Retrieves all the URI of a user

# **activate_uri**
> URI activate_uri(uri_id)

Activate a URI

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint


# create an instance of the API class
api_instance = swagger_client.UriApi(swagger_client.ApiClient(configuration))
uri_id = 'uri_id_example' # str | ID of URI to activate

try:
    # Activate a URI
    api_response = api_instance.activate_uri(uri_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UriApi->activate_uri: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uri_id** | **str**| ID of URI to activate | 

### Return type

[**URI**](URI.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **add_uri**
> URI add_uri(body)

Add a new URI

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint


# create an instance of the API class
api_instance = swagger_client.UriApi(swagger_client.ApiClient(configuration))
body = swagger_client.URIRequest() # URIRequest | URI that needs to be created

try:
    # Add a new URI
    api_response = api_instance.add_uri(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UriApi->add_uri: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**URIRequest**](URIRequest.md)| URI that needs to be created | 

### Return type

[**URI**](URI.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **deactivate_uri**
> URI deactivate_uri(uri_id)

Deactivate a URI

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint


# create an instance of the API class
api_instance = swagger_client.UriApi(swagger_client.ApiClient(configuration))
uri_id = 'uri_id_example' # str | ID of URI to deactivate

try:
    # Deactivate a URI
    api_response = api_instance.deactivate_uri(uri_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UriApi->deactivate_uri: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uri_id** | **str**| ID of URI to deactivate | 

### Return type

[**URI**](URI.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_uri**
> delete_uri(uri_id)

Deletes a URI

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint


# create an instance of the API class
api_instance = swagger_client.UriApi(swagger_client.ApiClient(configuration))
uri_id = 'uri_id_example' # str | URI id to delete

try:
    # Deletes a URI
    api_instance.delete_uri(uri_id)
except ApiException as e:
    print("Exception when calling UriApi->delete_uri: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uri_id** | **str**| URI id to delete | 

### Return type

void (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_hooks_by_uri**
> list[Hook] get_hooks_by_uri(uri_id)

Retrieves all the hooks of a URI

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint


# create an instance of the API class
api_instance = swagger_client.UriApi(swagger_client.ApiClient(configuration))
uri_id = 'uri_id_example' # str | ID of URI to get hooks from

try:
    # Retrieves all the hooks of a URI
    api_response = api_instance.get_hooks_by_uri(uri_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UriApi->get_hooks_by_uri: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uri_id** | **str**| ID of URI to get hooks from | 

### Return type

[**list[Hook]**](Hook.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_uri**
> URI get_uri(uri_id)

Find URI by ID

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint


# create an instance of the API class
api_instance = swagger_client.UriApi(swagger_client.ApiClient(configuration))
uri_id = 'uri_id_example' # str | ID of URI to return

try:
    # Find URI by ID
    api_response = api_instance.get_uri(uri_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UriApi->get_uri: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uri_id** | **str**| ID of URI to return | 

### Return type

[**URI**](URI.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_uri_by_user**
> list[URI] get_uri_by_user(user_id)

Retrieves all the URI of a user

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint


# create an instance of the API class
api_instance = swagger_client.UriApi(swagger_client.ApiClient(configuration))
user_id = 789 # int | ID of user to get URI from

try:
    # Retrieves all the URI of a user
    api_response = api_instance.get_uri_by_user(user_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UriApi->get_uri_by_user: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **int**| ID of user to get URI from | 

### Return type

[**list[URI]**](URI.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

