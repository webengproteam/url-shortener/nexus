# URIRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**real_uri** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

