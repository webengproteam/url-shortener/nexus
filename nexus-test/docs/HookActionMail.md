# HookActionMail

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**destinations** | **list[str]** |  | [optional] 
**subject** | **str** |  | [optional] 
**text** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

