# coding: utf-8

"""
    URL Shortener API

    This API is responsible of authetication and the creation, modification and deletion of uris and hooks.  # noqa: E501

    OpenAPI spec version: 1.0.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six


class Hook(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'hook_id': 'int',
        'user_id': 'int',
        'uri_id': 'str',
        'active': 'bool',
        'trigger': 'HookTrigger',
        'actions_http': 'list[HookActionHTTP]',
        'actions_mail': 'list[HookActionMail]'
    }

    attribute_map = {
        'hook_id': 'hookId',
        'user_id': 'userId',
        'uri_id': 'uriId',
        'active': 'active',
        'trigger': 'trigger',
        'actions_http': 'actionsHTTP',
        'actions_mail': 'actionsMail'
    }

    def __init__(self, hook_id=None, user_id=None, uri_id=None, active=None, trigger=None, actions_http=None, actions_mail=None):  # noqa: E501
        """Hook - a model defined in Swagger"""  # noqa: E501
        self._hook_id = None
        self._user_id = None
        self._uri_id = None
        self._active = None
        self._trigger = None
        self._actions_http = None
        self._actions_mail = None
        self.discriminator = None
        if hook_id is not None:
            self.hook_id = hook_id
        if user_id is not None:
            self.user_id = user_id
        if uri_id is not None:
            self.uri_id = uri_id
        if active is not None:
            self.active = active
        if trigger is not None:
            self.trigger = trigger
        if actions_http is not None:
            self.actions_http = actions_http
        if actions_mail is not None:
            self.actions_mail = actions_mail

    @property
    def hook_id(self):
        """Gets the hook_id of this Hook.  # noqa: E501


        :return: The hook_id of this Hook.  # noqa: E501
        :rtype: int
        """
        return self._hook_id

    @hook_id.setter
    def hook_id(self, hook_id):
        """Sets the hook_id of this Hook.


        :param hook_id: The hook_id of this Hook.  # noqa: E501
        :type: int
        """

        self._hook_id = hook_id

    @property
    def user_id(self):
        """Gets the user_id of this Hook.  # noqa: E501


        :return: The user_id of this Hook.  # noqa: E501
        :rtype: int
        """
        return self._user_id

    @user_id.setter
    def user_id(self, user_id):
        """Sets the user_id of this Hook.


        :param user_id: The user_id of this Hook.  # noqa: E501
        :type: int
        """

        self._user_id = user_id

    @property
    def uri_id(self):
        """Gets the uri_id of this Hook.  # noqa: E501


        :return: The uri_id of this Hook.  # noqa: E501
        :rtype: str
        """
        return self._uri_id

    @uri_id.setter
    def uri_id(self, uri_id):
        """Sets the uri_id of this Hook.


        :param uri_id: The uri_id of this Hook.  # noqa: E501
        :type: str
        """

        self._uri_id = uri_id

    @property
    def active(self):
        """Gets the active of this Hook.  # noqa: E501


        :return: The active of this Hook.  # noqa: E501
        :rtype: bool
        """
        return self._active

    @active.setter
    def active(self, active):
        """Sets the active of this Hook.


        :param active: The active of this Hook.  # noqa: E501
        :type: bool
        """

        self._active = active

    @property
    def trigger(self):
        """Gets the trigger of this Hook.  # noqa: E501


        :return: The trigger of this Hook.  # noqa: E501
        :rtype: HookTrigger
        """
        return self._trigger

    @trigger.setter
    def trigger(self, trigger):
        """Sets the trigger of this Hook.


        :param trigger: The trigger of this Hook.  # noqa: E501
        :type: HookTrigger
        """

        self._trigger = trigger

    @property
    def actions_http(self):
        """Gets the actions_http of this Hook.  # noqa: E501


        :return: The actions_http of this Hook.  # noqa: E501
        :rtype: list[HookActionHTTP]
        """
        return self._actions_http

    @actions_http.setter
    def actions_http(self, actions_http):
        """Sets the actions_http of this Hook.


        :param actions_http: The actions_http of this Hook.  # noqa: E501
        :type: list[HookActionHTTP]
        """

        self._actions_http = actions_http

    @property
    def actions_mail(self):
        """Gets the actions_mail of this Hook.  # noqa: E501


        :return: The actions_mail of this Hook.  # noqa: E501
        :rtype: list[HookActionMail]
        """
        return self._actions_mail

    @actions_mail.setter
    def actions_mail(self, actions_mail):
        """Sets the actions_mail of this Hook.


        :param actions_mail: The actions_mail of this Hook.  # noqa: E501
        :type: list[HookActionMail]
        """

        self._actions_mail = actions_mail

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(Hook, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, Hook):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
