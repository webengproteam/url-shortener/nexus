import os


class BaseConfig(object):
    PROXY_SERVICE = os.environ['PROXY_SERVICE']
    PROXY_PORT = os.environ['PROXY_PORT']
