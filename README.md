# Nexus

**Master:** [![pipeline status](https://gitlab.com/webengproteam/url-shortener/nexus/badges/master/pipeline.svg)](https://gitlab.com/webengproteam/url-shortener/nexus/commits/master)


Automation of integration testing and deployment of whole system.


## Subsystems state

- [![pipeline status](https://gitlab.com/webengproteam/url-shortener/api-rest-definition/badges/master/pipeline.svg)](https://gitlab.com/webengproteam/url-shortener/api-rest-definition/commits/master) **API REST definition**
- [![pipeline status](https://gitlab.com/webengproteam/url-shortener/api-rest-server/badges/master/pipeline.svg)](https://gitlab.com/webengproteam/url-shortener/api-rest-server/commits/master) **API REST server**
- [![pipeline status](https://gitlab.com/webengproteam/url-shortener/database-schema/badges/master/pipeline.svg)](https://gitlab.com/webengproteam/url-shortener/database-schema/commits/master) **Database**
- [![pipeline status](https://gitlab.com/webengproteam/url-shortener/redirector/badges/master/pipeline.svg)](https://gitlab.com/webengproteam/url-shortener/redirector/commits/master) **Redirector**
- [![pipeline status](https://gitlab.com/webengproteam/url-shortener/hook-runner/badges/master/pipeline.svg)](https://gitlab.com/webengproteam/url-shortener/hook-runner/commits/master) **Hook runner**
- [![pipeline status](https://gitlab.com/webengproteam/url-shortener/web-client/badges/master/pipeline.svg)](https://gitlab.com/webengproteam/url-shortener/web-client/commits/master) **Web client**
- [![pipeline status](https://gitlab.com/webengproteam/url-shortener/data-collector/badges/master/pipeline.svg)](https://gitlab.com/webengproteam/url-shortener/data-collector/commits/master) **Data collector**
- [![pipeline status](https://gitlab.com/webengproteam/url-shortener/uri-checker/badges/master/pipeline.svg)](https://gitlab.com/webengproteam/url-shortener/uri-checker/commits/master) **URI checker**


## Requirements

docker-compose

## Test

To test the system, just run:

```
./run_test.sh
```

## DoS protection

The proxy container runs Fail2Ban and limits the amount of requests to 60 per minute for each IP. In case of reaching that limit, the IP gets iptables-banned for 10 minutes. This limit can be changed with the env variable `IP_BAN_REQUEST_RATE` that is passed to the proxy container.

**NOTE:** in order to do this, docker-compose grants NET_ADMIN priviledges to the container.

## Usage

To set up the system run:

```bash
export DOMAIN=localhost
export MAIL_USER=my@mail.com
export MAIL_PASS=mypass
docker-compose up -d
```

MAIL_USER and MAIL_PASS are the credentials of the Gmail account to be used to rely the mails sent by the hooks. Note that in order for Gmail to allow this, you need to go to settings and enable the "Less secure apps" parameter.

Note that you need to explicitly define the base domain of the system. It can be `localhost`, but it will also work if you set a custom domain and configure the DNS resolving properly to reach the `proxy` container.

To bring the system down run:

```bash
docker-compose down
```

To update to the latest images run:

```bash
docker-compose pull
```

Once the system is set up you can navigate to the following endpoints (if using a custom domain, modify accordingly):

- **http://localhost/short-uri** to be redirected (substitute short-uri for your desired uri).
- **http://app.localhost/** to visit the web-client.
- **http://api.localhost/v3/ui** to make direct requests to the REST API.
- **http://data.localhost/graphql** to make direct requests to the GraphQL API.

## Design

### Front end

- <!-- .element: class="fragment" -->Redirector (Go)
- <!-- .element: class="fragment" -->Cache (Redis)
- <!-- .element: class="fragment" -->Web app (Vue.js)
- <!-- .element: class="fragment" -->Server REST (Swagger+Python)
- <!-- .element: class="fragment" -->Server GraphQL (Python)
- <!-- .element: class="fragment" -->Proxy (Nginx)

### Back end

- <!-- .element: class="fragment" -->Hook runner (Python)
- <!-- .element: class="fragment" -->URI checker (Bash+tool)
- <!-- .element: class="fragment" -->Database (PostgreSQL)

---

```mermaid
graph LR
    uc[URI Checker]
    sw[Web app]
    ar[API REST]
    ag[API GraphQL]
    he[Hook runner]
    fw[Proxy]
    re[Redirector]
    ca[Cache]
    bd[Database]
    sb[Safe browsing]
    oe[Other endpoints]
    
    fw --> re
    fw --> ar
    fw --> sw
    fw --> ag
    uc --> sb
    sw --> fw
    
    subgraph System
      uc --> bd
      re --> ca
      re --> bd
      ag --> bd
      ar --> bd
      he --> bd
    end
    
    he --> oe
```
