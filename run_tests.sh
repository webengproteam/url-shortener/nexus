export DOMAIN=proxy
docker-compose up -d
docker run --rm --network=nexus_default --env-file=.env registry.gitlab.com/webengproteam/url-shortener/nexus/tester:latest
docker-compose down -t 0
